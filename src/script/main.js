// 1) Щоб створити новий html тег на сторінці потрібно прописати: document.createElement('елемент який ми хочемо створити'). 
//Потім ми можемо додати йому клас за допомогою classList.add('назва класу') і додати його на сторінку за допомогою append
// 2) Перший параметр insertAdjacentHTML вказує позицію куди потрібно вставити елемент. Існує 4 позиції beforebegin, afterbegin, beforeend, afterend
// 3) Щоб видалити елемент потрібно прописати до того елемента який ми хочеми видалити remove()
const someArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];


function createList(arr, place = document.body) {
    let ul = document.createElement('ul');
    place.append(ul);
    arr.forEach(el => {
        let li = document.createElement('li');
        li.textContent = el;
        ul.append(li)
    });
}

createList(someArr)